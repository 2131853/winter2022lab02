import java.util.Scanner;

public class PartThree{

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Input first value");
		int x = reader.nextInt();
		System.out.println("Input second value");
		int y = reader.nextInt();
		System.out.println("Input third value");
		int z = reader.nextInt();
		
		int squareArea;
		squareArea = AreaComputations.areaSquare(x);
		System.out.println("area of square is " + squareArea);
		
		int rectangleArea;
		AreaComputations ac = new AreaComputations();
		rectangleArea = ac.areaRectangle(y,z);
		System.out.println("area of rectangle is " + rectangleArea);
	}
}